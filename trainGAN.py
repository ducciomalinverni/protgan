#!/usr/bin/env python3

import os
import sys
import time
import sequenceHandler as sh
import plotting

from tqdm.auto import tqdm
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
from torch.utils.data import DataLoader


def loadDataset(msaFile):
    """ Load and preprocess the dataset to model"""

    msa,_=sh.fastaToMatrix(msaFile)
    X=torch.from_numpy(sh.binarizeMSA(msa)).float()
    q=21
    N=int(X.shape[1]/q)
    return X,N,q

def initializeWeights(model):
    """ Initialize the model paraemters:uWeights bu normal (mu=0, s=0.02), bias to 0)"""

    if isinstance(model, nn.BatchNorm1d) or isinstance(model, nn.Linear):
        torch.nn.init.normal_(model.weight, 0.0, 0.02)
        torch.nn.init.constant_(model.bias, 0)


def get_noise(n_samples, z_dim, device='cpu'):
    """Function for creating noise vectors: Given the dimensions (n_samples, z_dim) """ 
    return torch.randn(n_samples, z_dim, device=device)
        
def getGradient(crit, real, fake, epsilon):
    """ Compute the gradient on mixed fake/real sequences for gradient penalty scheme"""

    mixed_sequences = real * epsilon + fake.flatten(start_dim=1) * (1 - epsilon)
    mixed_scores = crit(mixed_sequences)
    
    gradient = torch.autograd.grad(
        inputs=mixed_sequences,
        outputs=mixed_scores,
        grad_outputs=torch.ones_like(mixed_scores), 
        create_graph=True,
        retain_graph=True,
    )[0]
    return gradient

def getGradientPenalty(gradient):
    """ Return the gradient penalty, by RMSE to 1 of the gradients magnitues, computed on mixed real/fake sequences """

    gradient = gradient.view(len(gradient), -1)
    gradient_norm = gradient.norm(2, dim=1)
    penalty = ((gradient_norm-torch.ones_like(gradient_norm))**2).mean()
    return penalty

def get_gen_loss(crit_fake_pred):
    """ Return the loss of a generator given the critic's scores of the generator's fake images """
    return -crit_fake_pred.mean()

def get_crit_loss(crit_fake_pred, crit_real_pred, gradientPenalty, c_lambda):
    """ Return the loss of a critic given the critic's scores for fake and real images,
        the gradient penalty, and gradient penalty weight.
    """
    return -crit_real_pred.mean() + crit_fake_pred.mean() +c_lambda*gradientPenalty

class Generator(nn.Module):
    """ Generator class for sequence space GAN.
        Arguments:
           - N          (int):
           - q          (int):
           - z_dim      (int):
           - hidden_dim (int):
    """
    def __init__(self,N,q=21,z_dim=128,hidden_dim=64):
        super(Generator,self).__init__()
        self.z_dim=z_dim
        self.N=N
        self.q=q
        self.gen = nn.Sequential(
            self.make_block(self.z_dim,hidden_dim*10),
            self.make_block(hidden_dim*10,hidden_dim*6),
            self.make_block(hidden_dim*6,hidden_dim*4),
            self.make_block(hidden_dim*4,hidden_dim*2),
            nn.Linear(hidden_dim*2,N*q)
        )
                      
    def make_block(self,in_dim,out_dim):
        return nn.Sequential(
            nn.Linear(in_dim,out_dim),
            nn.BatchNorm1d(out_dim),
            nn.LeakyReLU(0.2,inplace=True)
        )
    
    def forward(self,noise):
        y = self.gen(noise)
        return nn.Softmax(dim=2)(y.view(y.shape[0],self.N,self.q))   

class Critic(nn.Module):
    """ Critic class for sequence space GAN.
        Arguments:
           - N (int):
           - q (int):
           - hidden_dim (int):
    """
    def __init__(self,N,q,hidden_dim=64):
        super(Critic,self).__init__()
        self.N=N
        self.q=q
        self.crit=nn.Sequential(
            self.make_block(N*q,hidden_dim*16),
            self.make_block(hidden_dim*16,hidden_dim*8),
            self.make_block(hidden_dim*8,hidden_dim*4),
            self.make_block(hidden_dim*4,hidden_dim*2),
            nn.Linear(hidden_dim*2,1)
            )

    def make_block(self,in_dim,out_dim):
        return nn.Sequential(nn.Linear(in_dim,out_dim),
                             nn.BatchNorm1d(out_dim),
                             nn.LeakyReLU(0.2,inplace=True)
                             )
    
    def forward(self,sequence):
        return self.crit(sequence.flatten(start_dim=1))
    
if __name__=="__main__":   
    msa=sys.argv[1]
    sequences,N,q = loadDataset(msa)

   
    # Parameters
    n_epochs = 300
    z_dim = 64
    display_step = 50
    batch_size = 128
    lr = 0.0002
    beta_1 = 0.5
    beta_2 = 0.999
    c_lambda = 1
    crit_repeats = 1
    gen_repeats = 5
    device = 'cpu'  
    hidden_dim_gen=64
    hidden_dim_crit=64
    
    
    # Setup data flux and GAN networks
    dataloader = DataLoader(sequences,batch_size,shuffle=True)
    gen = Generator(N,q,z_dim,hidden_dim_gen).to(device)
    gen_opt = torch.optim.Adam(gen.parameters(), lr=lr, betas=(beta_1, beta_2))
    crit = Critic(N,q,hidden_dim_crit).to(device) 
    crit_opt = torch.optim.Adam(crit.parameters(), lr=lr, betas=(beta_1, beta_2))
    gen = gen.apply(initializeWeights)
    crit = crit.apply(initializeWeights)

    # Train the GAN
    cur_step = 0
    generator_losses = []
    critic_losses = []
    for epoch in range(n_epochs):
        for real in tqdm(dataloader):
            cur_batch_size = len(real)
            real = real.to(device)
            mean_iteration_critic_loss = 0
            for _ in range(crit_repeats):
                ### Update critic ###
                crit_opt.zero_grad()
                fake_noise = get_noise(cur_batch_size, z_dim, device=device)
                fake = gen(fake_noise)
                crit_fake_pred = crit(fake.detach())
                crit_real_pred = crit(real)
                
                epsilon = torch.rand(len(real), 1,  device=device, requires_grad=True)
                gradient = getGradient(crit, real, fake.detach(), epsilon)
                gp = getGradientPenalty(gradient)
                crit_loss = get_crit_loss(crit_fake_pred, crit_real_pred, gp, c_lambda)

                # Keep track of the average critic loss in this batch
                mean_iteration_critic_loss += crit_loss.item() / crit_repeats
                # Update gradients
                crit_loss.backward(retain_graph=True)
                # Update optimizer
                crit_opt.step()
            critic_losses += [mean_iteration_critic_loss]

            ### Update generator ###
            for _ in range(gen_repeats):
                gen_opt.zero_grad()
                fake_noise_2 = get_noise(cur_batch_size, z_dim, device=device)
                fake_2 = gen(fake_noise_2)
                crit_fake_pred = crit(fake_2)
        
                gen_loss = get_gen_loss(crit_fake_pred)
                gen_loss.backward()
            
                # Update the weights
                gen_opt.step()
            
            # Keep track of the average generator loss
            generator_losses += [gen_loss.item()]

            ### Visualization code ###
            if cur_step % display_step == 0 and cur_step > 0:
                gen_mean = sum(generator_losses[-display_step:]) / display_step
                crit_mean = sum(critic_losses[-display_step:]) / display_step
                print(f"Step {cur_step}: Generator loss: {gen_mean}, critic loss: {crit_mean}")
                step_bins = 20
                num_examples = (len(generator_losses) // step_bins) * step_bins
                plt.figure()
                plt.plot(range(num_examples // step_bins), torch.Tensor(generator_losses[:num_examples]).view(-1, step_bins).mean(1), label="Generator Loss")
                plt.plot(range(num_examples // step_bins), torch.Tensor(critic_losses[:num_examples]).view(-1, step_bins).mean(1), label="Critic Loss")
                plt.legend()
                plt.savefig('runOut/losses.pdf')

                gen.eval()
                with torch.no_grad():
                    generatedSequences = gen(get_noise(2000,z_dim,device=device)).argmax(dim=2)
                    sh.matrixToFasta(generatedSequences,'runOut/msa'+str(cur_step)+'.fasta')
                gen.train()
                plt.close('all')
            cur_step += 1
