# protGAN
Generative Adversarial Network for building generative models of protein sequence families.

**protGAN** trains a GAN over a protein family encoded in its multiple-sequence alignment. It is based on a Wasserstein GAN formulation, with gradient Penalty.
Both the Generator and Critic are built of fully connected blocks, each composed of Linear unit, Batch Normalization and a Leaky ReLU activation.
By default, the generator is updated 5 times per iterations, while the critic is updated once.
All training parameters can be tuned at lines 137-149 oif trainGAN.py.


# Requirements
  * [dcaTools](https://gitlab.com/ducciomalinverni/dcaTools)
  * pytorch >=1.7.0
  * tqdm >=4.51.0
  * matplotlib >=3.2.0

# Usage
To train a model on an MSA, call

```shell
$ ./trainGAN.py msa.fasta
```

By default, the Generator and Critic losses are plotted every training 50 steps. A set of 2000 synthetic sequences is also generated every 50 steps.



  